package com.zyy.gulimall.ware.dao;

import com.zyy.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zyy
 * @email zyy@gmail.com
 * @date 2020-10-23 10:06:41
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
